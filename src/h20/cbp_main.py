import sys
import configparser
from queue import Queue
import traceback

from qpython import qconnection

from cbp_kdb import KdbThread
from cbp_service import CbpService
from cbp_utils import *


def usage():
    print("usage: python cbp_data.py trade BTC-USD")
    print("    or python cbp_data.py quote BTC-USD")
    print("                                       ")
    sys.exit(0)


def usageQ(host, port):
    print("INFO: make sure you have Kdb service running.")
    print("      start a q service on {}:{}".format(host, port))
    sys.exit(0)


# global properties
config = {}


# define message queue to be shared between WSS and Kdb+ threads
queue = Queue()


if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage()

    try:
        config = load_app_config('app.config')

        # append commandline inputs
        config['channels'] = sys.argv[1]
        config['products'] = sys.argv[2]

        if eval(config['debug']):
            print('program settings: ', config)

        # check if Q service running
        q = init_q(config['kdb_host'], int(config['kdb_port']))

        if q is None:
            usageQ(config['kdb_host'], int(config['kdb_port']))

        tables = load_q_script(q, 'cbp_tables.q')
        print('kdb tables: ', tables)
        load_q_script(q, 'json.k')

        # load the tables from /tmp dir
        table = config['channels']
        load_kdb_tables(q, table)

        # start Kdb+ data thread
        kt = KdbThread(config, q, queue)
        kt.start()

        # start data feed service
        # websocket.enableTrace(True)
        cbp_service = CbpService(config, queue)
        cbp_service.start()

        print('xxxx real-time data service ended, stopping KdbThread...')
        kt.stop()
        kt.join()

        # we want to save the table before exiting
        save_kdb_tables(q, table)

        print('....main exiting...0')
        sys.exit()

    except Exception as e:
        print('....main program exceptions: ', e)
        traceback.print_exc(file=sys.stdout)

    except KeyboardInterrupt:
        print('....main program exiting (killed by user)...')
        sys.exit(-1)
