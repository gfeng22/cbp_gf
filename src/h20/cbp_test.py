from unittest import TestCase, main

# from h20.cbp_data import to_str, load_app_config, config
# from h20.cbp_data import init_q, load_kdb_tables, save_kdb_tables, get_table_columns, config, load_q_script


class CbpTestCase(TestCase):
    pass

    """
    def test_to_str_bytes(self):
        self.assertEqual('hello', to_str(b'hello'))

    # test property file
    def test_load_app_config(self):
        load_app_config("app.config")

        self.assertEqual(config['cbp_url'], "wss://ws-feed.pro.coinbase.com")
        self.assertEqual(config['kdb_host'], "localhost")
        self.assertEqual(config['kdb_port'], "5001")

    # q service test
    def test_init_q(self):
        load_app_config("app.config")
        host = config['kdb_host']
        port = int(config['kdb_port'])
        q = init_q(host, port)

        self.assertTrue(q.is_connected())
        q.close()

    def test_load_q_script(self):
        load_app_config("app.config")
        host = config['kdb_host']
        port = int(config['kdb_port'])
        q = init_q(host, port)

        results = load_q_script(q, 'cbp_tables.q')
        q.close()

        tables = [to_str(x) for x in results]
        self.assertTrue('trade' in tables)
        self.assertTrue('quote' in tables)


    def test_load_kdb_tables(self):
        load_app_config("app.config")
        host = config['kdb_host']
        port = int(config['kdb_port'])
        q = init_q(host, port)

        load_kdb_tables(q, 'trade')
        tc = q('count trade')
        q.close()

        self.assertTrue(tc>=0)

    def test_get_table_columns_trade(self):
        load_app_config("app.config")
        host = config['kdb_host']
        port = int(config['kdb_port'])
        q = init_q(host, port)

        cols = get_table_columns(q, 'trade')
        q.close()

        self.assertEqual(cols, ['sym','time','price'])

    def test_get_table_columns_quote(self):
        load_app_config("app.config")
        host = config['kdb_host']
        port = int(config['kdb_port'])
        q = init_q(host, port)

        cols = get_table_columns(q, 'quote')
        q.close()

        self.assertEqual(cols, ['sym','time','bid','ask'])

    """


if __name__ == '__main__':
    main()
