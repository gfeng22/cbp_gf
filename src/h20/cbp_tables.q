/ data tables

ticker:flip `sym`time`price!"szf"$\:();
level2:flip `sym`time`bid`ask!"szff"$\:();

/
 function to generate outlier trades, or price outside of bid,ask
 sym: currency pair, i.e. BTC-USD
 t: ticker
 q: level2 
 st: start time
 et: end time

 test: chk_px_outliers[`$"BTC-USD";`ticker;`level2;2019.12.06T03:00:00;2019.12.31T03:00:00]
\

chk_px_outliers:{[sym;t;q;st;et]
 / get data from ticker, level2
 tdata:0!select from t where sym=sym, time within (st;et);
 qdata:0!select from q where sym=sym, time within (st;et);

 / merge trade and quote by sym,time
 tq:aj[`sym`time;tdata;update fills bid, fills ask from qdata];
 pxchk:select from tq where not price within (bid;ask);
 pxagg:select num_of_voilations:count i, px_vio_stime:first time, px_vio_etime:last time by sym, price, bid, ask from pxchk;

 pxagg
 }

save_table:{
 fd:"" sv ("/tmp/";(string x);".dat");
 (hsym `$fd) set (value x);
 }

/ .z.ts:{save_table each tables[]}

\t 5000
