import os
import sys

import configparser

from qpython import qconnection


# sample kdb test data
def to_str(data):
    if isinstance(data, str):
        return data
    elif isinstance(data, bytes):
        return data.decode('utf-8')
    else:
        raise TypeError('invalid input data: ', data)


# config utils
def load_app_config(config_file):
    config = {}

    try:
        appconfig = configparser.ConfigParser()
        base_dir = os.path.abspath(os.path.dirname('__file__'))
        appconfig.read(os.path.join(base_dir, config_file))

        for c in appconfig['DEFAULT']:
            config[c] = appconfig['DEFAULT'][c]

        print("DEFAULT configs: ", config)

    except Exception as e:
        raise Exception('Error init/start Services. %s' % e)

    return config


# init q service - start if not running
def init_q(host, port):
    try:
        q = qconnection.QConnection(host, port)
        q.open()
        print(q)
        print('IPC version: %s. Is connected: %s' % (q.protocol_version, q.is_connected()))

        # send a handshake message
        print('xxxx kdb time: ', q('.z.z'))

        return q

    except Exception as e:
        print('init Q error: ', e)

    return None


def load_q_script(q, qfile):

    try:
        base_dir = os.path.abspath(os.path.dirname('__file__'))
        fpath = os.path.join(base_dir, qfile)

        print('$$$$ loading Q script {} to kdb...'.format(fpath))

        q('\\l {}'.format(fpath))

        return q('tables[]')

    except Exception as e:
        print('load_kdb_script error: ', e)


def load_kdb_tables(q, table_name):
    print('$$$$ loading [{}] table from disk...'.format(table_name))

    try:
        q('if [not ()~key `:/tmp/{}.dat; {}: get `:/tmp/{}.dat]'.format(table_name, table_name, table_name))
        print('xxxx rows in {}:'.format(table_name))
        print(q('count {}'.format(table_name)))
    except Exception as e:
        print('load_kdb_tables error: ', e)


def save_kdb_tables(q, table_name):
    print('$$$$ saving table to disk...')

    try:
        print(q('`:/tmp/{}.dat set {}'.format(table_name, table_name)))
        print('xxxx rows in {}:'.format(table_name))
        print(q('count {}'.format(table_name)))
    except Exception as e:
        print('load_kdb_tables error: ', e)


def get_table_columns(q, table_name):
    try:
        return [x.decode('utf-8') for x in q('cols {}'.format(table_name))]
    except Exception as e:
        print('load_kdb_tables error: ', e)

    return None
