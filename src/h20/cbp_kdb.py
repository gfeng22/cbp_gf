import numpy as np
import json
import datetime
import traceback
import itertools
# from collections import OrderedDict
from sortedcontainers import SortedDict
from datetime import datetime

import threading
from threading import Thread
from queue import Queue
import time

from qpython import qconnection
from qpython import qcollection
from qpython.qcollection import QDictionary, qlist, QFLOAT_LIST

from cbp_utils import *


# kdb+ data thread
class KdbThread(Thread):

    def __init__(self, config, q, queue):
        super(KdbThread, self).__init__()

        self.q = q
        self.queue = queue
        self._stopper = threading.Event()

        self.table = config['channels']
        self.cols = get_table_columns(self.q, self.table)
        self.debug = eval(config['debug'])

        # price levels for level2
        self.bids = SortedDict()
        self.asks = SortedDict()

        self.best_bid = 0.0
        self.best_ask = 0.0

    def stop(self):
        self._stopper.set()

    def stopped(self):
        return self._stopper.isSet()

    def publish_to_kdb(self, df):
        # load json into dataframe
        df = df.reset_index(drop=True)

        try:
            # kdb data obj
            kdf = df[self.cols]
            self.q.sendAsync('{y insert x}', kdf, np.string_(self.table))

            if self.debug:
                print('$$$$ KKKK debug insert data in kdb: ', kdf)

        except Exception as e:
            print('xxx processing to Kdb error: ', e)
            traceback.print_exc(file=sys.stdout)

    def parse_level2(self, msg_json, side, depth=20):
        levels = msg_json[side]

        # iterate levels to get the full depth
        price_levels = SortedDict()
        for i, l in enumerate(levels):
            price_levels[np.float(l[0])] = np.float(l[1])

            # only take top x levels
            if i > depth:
                break

        return price_levels

    def update_book(self, msg_json):
        # update self.bids, self.asks levels
        changes = msg_json['changes']

        for update in changes:
            side, price, size = update[0], np.float(update[1]), np.float(update[2])

            if 'sell' == side:
                if size > 0.0:
                    self.asks[price] = size

                elif price in self.asks:
                    del self.asks[price]

            elif 'buy' == side:
                if size > 0.0:
                    self.bids[price] = size

                    if self.debug:
                        print('XXXXXXXXXX CHECK book after BUY:')
                        self.print_levels(self.bids, len(self.bids))
                        print('XXXXXXXXXX CHECK book DONE:')

                elif price in self.bids:
                    del self.bids[price]

            else:
                print('XXXX Unkown level2 update values: ', update)

    def update_book_best(self, msg_json):
        # compare the updates with existing bid, ask
        changes = msg_json['changes']

        for update in changes:
            side, price, size = update[0], np.float(update[1]), np.float(update[2])

            if 'sell' == side:
                if size > 0.0 and price < self.best_ask:
                    self.best_ask = price

            elif 'buy' == side:
                if size > 0.0 and price > self.best_bid:
                    self.best_bid = price

            else:
                print('XXXX Unkown level2 update values: ', update)

    def print_levels(self, levels, depth=10):
        # check book integrity
        book = itertools.islice(levels.items(), 0, depth)
        for k, v in book:
            print('{}->{}'.format(k, v))

    def upd_kdb_dict(self, data):
        if self.debug:
            print('$$$$ KKKK {} debug insert data in kdb: {}'.format(datetime.now(), data))

        try:
            self.q.sendAsync('{t:update sym:`$sym, time:"Z"$time from enlist .j.k x; y insert t}',
                             data, np.string_(self.table))

            if self.debug:
                print('xxxx debug sent json data to kdb: ', data)

        except Exception as e:
            print('xxx processing to Kdb error: ', e)
            traceback.print_exc(file=sys.stdout)

    def process(self, msg_json):
        # we only care about 3 messages: ticker, level2: (snapshot, l2update)

        try:
            msg_type = msg_json['type']

            if 'ticker' == msg_type:
                data = {
                    'sym': msg_json['product_id'],
                    'time': msg_json["time"],
                    'price': np.float(msg_json["price"])
                }

                self.upd_kdb_dict(json.dumps(data))

            elif 'snapshot' == msg_type:
                # initialize bids, asks book
                self.bids = self.parse_level2(msg_json, 'bids', 100).copy()
                self.asks = self.parse_level2(msg_json, 'asks', 100).copy()

                # send the best bid,ask to kdb
                self.best_bid = self.bids.keys()[-1]
                self.best_ask = self.asks.keys()[0]

                # kdb json obj
                data = {
                    'sym': msg_json['product_id'],
                    'time': str(datetime.now()),
                    'bid': self.best_bid,
                    'ask': self.best_ask
                }

                # self.publish_to_kdb(data[self.cols])
                self.upd_kdb_dict(json.dumps(data))

            elif 'l2update' == msg_type:
                stime = datetime.now()

                # compare the updates with existing bid, ask
                self.update_book(msg_json)

                self.best_bid = self.bids.keys()[-1]
                self.best_ask = self.asks.keys()[0]

                # kdb json obj
                data = {
                    'sym': msg_json['product_id'],
                    'time': msg_json["time"],
                    'bid': self.best_bid,
                    'ask': self.best_ask
                }

                # self.publish_to_kdb(data[self.cols])
                self.upd_kdb_dict(json.dumps(data))

            elif 'subscriptions' == msg_type:
                print('status Subscribed to: ', msg_json['channels'])

            else:
                print('XXXX Unkown message type: ', msg_json)

        except Exception as e:
            print("Error processing json message: ", e)
            traceback.print_exc(file=sys.stdout)

    def run(self):
        print('xxxx kdb data thread starting...')

        while not self.stopped():
            try:
                if self.queue.qsize() > 0:
                    if self.debug:
                        print('xxxx QQQQ-{} kdb data queue [{}] size: {}'.format(datetime.now(),
                              self.table, self.queue.qsize()))

                    data = self.queue.get()
                    self.process(data)

            except Exception as e:
                print(e)
                # time.sleep(0.2)
                self.stop()

        print('xxxx kdb data thread exiting...')
