import os
import sys
import numpy as np
import pandas as pd

import datetime
import json
import threading
import random
from threading import Thread
from queue import Queue
import configparser

import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time

from qpython import qconnection


# sample kdb test data
def to_str(data):
    if isinstance(data, str):
        return data
    elif isinstance(data, bytes):
        return data.decode('utf-8')
    else:
        raise TypeError('invalid input data: ', data)


# kdb+ data thread
class KdbThread(Thread):

    def __init__(self, q, queue):
        super(KdbThread, self).__init__()
        self.q = q
        self.queue = queue
        self._stopper = threading.Event()

    def stop(self):
        self._stopper.set()

    def stopped(self):
        return self._stopper.isSet()

    def process(self, json):
        # load json into dataframe
        try:
            ticker_df = pd.DataFrame.from_dict([json])
            df = ticker_df[['product_id', 'time', 'price', 'best_bid', 'best_ask']]
            df = df.reset_index(drop=True)

            # convert to Kdb+ types
            df['sym'] = df['product_id']
            df['time'] = pd.to_datetime(df['time'])
            df['price'] = np.float(df['price'])
            df['bid'] = np.float(df['best_bid'])
            df['ask'] = np.float(df['best_ask'])

            print(df)
            # print(df.dtypes)

            cols = get_table_columns(q, config['table'])
            q.sendAsync('{y insert x}', df[cols], np.string_(config['table']))

        except Exception as e:
            print('xxx processing to Kdb error: ', e)

    def run(self):
        print('xxxx kdb data thread starting...')

        while not self.stopped():
            try:
                tick = self.queue.get()
                print('xxxx Kdb thread got tick: ', tick['time'])
                self.process(tick)

                time.sleep(0.1)
            except Exception as e:
                print(e)
                time.sleep(0.2)
                self.stop()

        print('xxxx kdb data thread exiting...')


# wss callbacks
def on_message(ws, message):
    print('xxxx ws:{} received: {}, {}'.format(ws, type(message), message))

    try:
        msg_json = json.loads(message)
        # print('xxxx json obj: {}, {}'.format(type(msg_json), msg_json))

        # publish this to the queue for kdb thread to process
        if 'ticker' in msg_json['type']:
            queue.put(msg_json)

    except Exception as e:
        print('xxxx on_message error: ', e)


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    def run(*args):
        print('xxxx starting marketdata thread @ ', datetime.time())

        # send subscription request
        # sub_req = { "type": "subscribe", "product_ids": ["BTC-USD"], "channels": ["ticker", "heartbeat"] }
        sub_req = {"type": "subscribe", "product_ids": [config['product']], "channels": ["ticker", "heartbeat"]}
        print('XXXX sub_req: ', sub_req)

        ws.send(json.dumps(sub_req))

        # checking ws connection
        try:
            while ws.connected:
                print('xxxx wss session is live.')
                time.sleep(5)
        except Exception as e:
            print('wss session erriro: ', e)

        print("xxxx wss marketdata thread terminating...")

    thread.start_new_thread(run, ())


# config utils
def load_app_config(config_file):
    try:
        appconfig = configparser.ConfigParser()
        base_dir = os.path.abspath(os.path.dirname('__file__'))
        appconfig.read(os.path.join(base_dir, config_file))

        for c in appconfig['DEFAULT']:
            config[c] = appconfig['DEFAULT'][c]

        print("DEFAULT configs: ", config)
    except Exception as e:
        raise Exception('Error init/start Services. %s' % e)


# init q service - start if not running
def init_q(host, port):
    try:
        q = qconnection.QConnection(host, port)
        q.open()
        print(q)
        print('IPC version: %s. Is connected: %s' % (q.protocol_version, q.is_connected()))

        # send a handshake message
        print('xxxx kdb time: ', q('.z.z'))

        return q

    except Exception as e:
        print('init Q error: ', e)
        usage()

    return None


def load_q_script(q, qfile):

    try:
        base_dir = os.path.abspath(os.path.dirname('__file__'))
        fpath = os.path.join(base_dir, qfile)

        print('$$$$ loading Q script {} to kdb...'.format(fpath))

        q('\\l {}'.format(fpath))

        return q('tables[]')

    except Exception as e:
        print('load_kdb_script error: ', e)


def load_kdb_tables(q, table_name):
    print('$$$$ loading [{}] table from disk...'.format(table_name))

    try:
        # q('if [not ()~key `:/tmp/trade.dat; trade: get `:/tmp/trade.dat]')
        q('if [not ()~key `:/tmp/{}.dat; {}: get `:/tmp/{}.dat]'.format(table_name, table_name, table_name))
        print('xxxx rows in {}:'.format(table_name))
        print(q('count {}'.format(table_name)))
    except Exception as e:
        print('load_kdb_tables error: ', e)


def save_kdb_tables(q, table_name):
    print('$$$$ saving table to disk...')

    try:
        print(q('`:/tmp/{}.dat set {}'.format(table_name, table_name)))
        print('xxxx rows in {}:'.format(table_name))
        print(q('count {}'.format(table_name)))
    except Exception as e:
        print('load_kdb_tables error: ', e)


def get_table_columns(q, table_name):
    try:
        return [x.decode('utf-8') for x in q('cols {}'.format(table_name))]
    except Exception as e:
        print('load_kdb_tables error: ', e)

    return None


def usage():
    print("usage: python cbp_data.py trade BTC-USD")
    print("    or python cbp_data.py quote BTC-USD")
    print("                                       ")
    print("make sure you have Kdb service running.")
    print("                                       ")
    sys.exit(0)


# global properties
config = {}

# define message queue to be shared between WSS and Kdb+ threads
queue = Queue()


if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage()

    try:
        load_app_config('app.config')

        # append commandline inputs
        config['table'] = sys.argv[1]
        config['product'] = sys.argv[2]
        # config['table'] = 'trade'
        # config['product'] = 'BTC-USD'

        print('program settings: ', config)

        # check if Q service running
        q = init_q(config['kdb_host'], int(config['kdb_port']))
        if q is None:
            usage()

        load_q_script(q, 'cbp_tables.q')

        # load the tables from /tmp dir
        load_kdb_tables(q, config['table'])
        # load_kdb_tables(q, 'quote')

        # start Kdb+ data thread
        kt = KdbThread(q, queue)
        kt.start()

        # websocket service
        websocket.enableTrace(True)
        ws = websocket.WebSocketApp(config['cbp_url'], on_message=on_message, on_error=on_error, on_close=on_close)

        # init sub-req
        ws.on_open = on_open
        ws.run_forever()

        print('xxxx wss ended: ', ws)

        # stop Kdb thread
        kt.stop()
        # kt.join()

        # we want to save the tables before exiting
        save_kdb_tables(q, config['table'])
        # save_kdb_tables(q, 'quote')

        # clean up q service
        print('xxxx closing q handle...', q)
        q.close()

        print('xxxx main exiting...0')
        sys.exit()

    except Exception as e:
        print('....program exceptions, exiting...', e)
        sys.exit(-1)
