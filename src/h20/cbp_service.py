import os
import sys
import datetime
import json

import threading
from threading import Thread
from queue import Queue

import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time


class CbpService(object):
    """
    subscribe to real-time market data:

    inputs:
        - currency pair: BTC-USD, ETH-USD
        - channel: ticker or level2
    """
    def __init__(self, config, queue):
        self.config = config
        self.queue = queue

        # init ws
        self.url = config['cbp_url']
        self.ws = websocket.WebSocketApp(
            self.url,
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close
        )

        self.debug = eval(self.config['debug'])

    # wss callbacks
    def on_message(self, message):
        if self.debug:
            print('xxxx ws:{} received: {}, {}'.format(self.ws, type(message), message))

        try:
            msg_json = json.loads(message)

            if self.debug:
                print('xxxx json obj: {}, {}'.format(type(msg_json), msg_json))

            # publish this to the queue for kdb thread to process
            self.queue.put(msg_json)

        except Exception as e:
            print('xxxx on_message error: ', e)

    def on_error(self, error):
        print("xxxx wss error: ", error)

    def on_close(self):
        print("### closed ###")

    def on_open(self):
        def run(*args):
            print('xxxx starting marketdata thread @ ', datetime.datetime.now())

            # send subscription request
            # sub_req = { "type": "subscribe", "product_ids": ["BTC-USD"], "channels": ["ticker", "heartbeat"] }

            products = self.config['products'].split(",")
            channels = self.config['channels'].split(",")
            sub_req = {"type": "subscribe", "product_ids": products, "channels": channels}

            print('XXXX sending sub_req: ', sub_req)
            self.ws.send(json.dumps(sub_req))

        thread.start_new_thread(run, ())

    def start(self):
        try:
            # init sub-req
            self.ws.on_open = self.on_open
            self.ws.run_forever()

        except Exception as e:
            print('Error connecting to data service: ', e)
            sys.exit(-2)
