# georgef

# running instructions: 

modules: 
    cbp_service.py: 
        subscribe to coinbase pro for real-time market data.  "ticker" for trades, "level2" for quotes.
    
    cbp_kdb.py:
        thread to process data to kdb

    cbp_utils.py: 
        util functions
    
    cbp_tables.q 
        q functions and table schema

    cbp_main.py: 
        main program to run

a q service need to be running on the same host to test the program.

to run: 

python -r requirements.txt 

to capture trades: python cbp_main.py ticker BTC-USD

to capture quotes: python cbp_main.py level2 BTC-USD
