from setuptools import setup, find_packages

setup(
    name='h20',
    version='0.0.1',
    description='H20: George F',
    url='https://gitlab.com/enlnt/h20',
    author='Aleks Bunin',
    author_email='b@enlnt.com',
    license='Proprietary',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    install_requires=[
        'requests',
    ],
    include_package_data=True,
    entry_points={
        'console_scripts': ['h20=h20.__main__:main'],
    },
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
)
